package com.delta.springnotes.repos;

import com.delta.springnotes.models.Ad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AdRepository extends JpaRepository<Ad, Long> {

    // the following method is equivalent to the built-in 'getOne' method, just to demonstrate that we can do it here to.

    @Query("from Ad a where a.id like ?1")
    Ad getAdById(long id);
}
