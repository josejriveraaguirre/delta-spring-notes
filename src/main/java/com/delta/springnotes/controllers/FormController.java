package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormController {
    @GetMapping("/show-form")
    public String showForm() {
        return "company-form";
    }
    @PostMapping("/process-form")

    public String displayForm(@RequestParam(name="first_name")String firstName,
                              @RequestParam(name="last_name")String lastName,
                              @RequestParam(name="city")String city,
                              @RequestParam(name="state")String state,
                              @RequestParam(name="email_address")String emailAddress, Model model) {

        model.addAttribute("first_name",firstName);
        model.addAttribute("last_name",lastName);
        model.addAttribute("city",city);
        model.addAttribute("state",state);
        model.addAttribute("email_address",emailAddress);

        return "show-form-submit";
    }
}
