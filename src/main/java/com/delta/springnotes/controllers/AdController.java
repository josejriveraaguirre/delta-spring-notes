package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Ad;
import com.delta.springnotes.repos.AdRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@Controller
public class AdController {

    // spring boot's dependency injection

    private final AdRepository adRepository;

    public AdController(AdRepository adRepository) {
        this.adRepository = adRepository;
    }

    // gets all the ads and displays them in "ads/index" view.

    @GetMapping("/ads")
    public String showAds(Model model) {

        List<Ad> adList = adRepository.findAll();
        model.addAttribute("noAdsFound", adList.size() == 0);
        model.addAttribute("ads", adList);
        return "ads/index"; // name of the view

    }

    // show the create/add an ad view

    @GetMapping("/ads/create")
        public String showForm(Model model) {
        model.addAttribute("newAd", new Ad());
        return "ads/create";
    }

    @PostMapping("/ads/create")
    public String createAd(@ModelAttribute Ad adToCreate) {
        // save the adToCreate parameter
        adRepository.save(adToCreate);
        // redirect the user to the list of ads -> /ads
        return "redirect:/ads/";
    }

    // controller method to display an individual ad
    @GetMapping("/ads/{id}")
    public String showAd(@PathVariable long id, Model model) {
        Ad ad = adRepository.getOne(id);
        model.addAttribute("showAd", ad);
        return "/ads/show";
    }

    // controller method that will allow for our user to edit
    @GetMapping("ads/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find the ad object with the passed in id
        Ad adToEdit = adRepository.getOne(id);
        model.addAttribute("editAd", adToEdit);
        return "ads/edit";
    }

    @PostMapping("/ads/{id}/edit")
    public String updateAd(@PathVariable long id,
                           @RequestParam(name ="title") String title,
                           @RequestParam(name = "description") String description) {

        // find the ad with the passed in id
        Ad foundAd = adRepository.getAdById(id); //NOTE in mySQL -> SELECT * FROM ads WHERE id = ?

        // update the ad's title and description
        foundAd.setTitle(title);
        foundAd.setDescription(description);

        // save the new ad's data changes
        adRepository.save(foundAd);

        // redirect user to the url that contains the list of ads
        return "redirect:/ads/";
    }

    // controller method to delete objects of our class
    @PostMapping("/ads/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deleteById() method from the repository
        adRepository.deleteById(id);

        // redirect user to url for list of ads
        return "redirect:/ads/";
    }

} // end of class
