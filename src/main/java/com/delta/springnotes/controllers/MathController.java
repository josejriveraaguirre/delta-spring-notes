package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MathController {

    @GetMapping("/add/{num1}/and/{num2}")
    @ResponseBody
    public String add(@PathVariable int num1, @PathVariable int num2) {
        return num1+"plus"+num2+"is"+(num1+num2);
    }

    // another add method
    @GetMapping("/add/{num1}/{num2}")
    @ResponseBody
    public int addNumbers(@PathVariable int num1, @PathVariable int num2) {
        return num2-num1;
    }

    @GetMapping("/subtract/{num1}/from/{num2}")
    @ResponseBody
    public String subtract(@PathVariable int num1, @PathVariable int num2) {
        return num1+" minus "+num2+" is "+(num2-num1);
    }

    // another add method
    @GetMapping("/subtract/{num1}/{num2}")
    @ResponseBody
    public int subtractNumbers(@PathVariable int num1, @PathVariable int num2) {
        return num1-num2;
    }

    @GetMapping("/multiply/{num1}/{num2}")
    @ResponseBody
    public int multiplyNumbers(@PathVariable int num1, @PathVariable int num2) {
        return num1*num2;
    }

    @GetMapping("/divide/{num1}/{num2}")
    @ResponseBody
    public int divideNumbers(@PathVariable int num1, @PathVariable int num2) {
        return num1/num2;
    }


}// end of class
