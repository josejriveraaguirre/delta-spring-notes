package com.delta.springnotes.services;

import com.delta.springnotes.models.User;
import com.delta.springnotes.models.UserWithRoles;
import com.delta.springnotes.repos.Users;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/* We defined a service thatSpring Security will use to load the authentication and authorization information of users.*/

@Service
public class UserDetailsLoader implements UserDetailsService {

    private final Users users;
    public UserDetailsLoader(Users users) {
        this.users = users;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = users.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user found for " + username);
        }
        return new UserWithRoles(user);
    }

}
