package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Book;
import com.delta.springnotes.repos.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BookController {

    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping("/books")
    public String showBooks(Model model) {
        List<Book> bookList = bookRepository.findAll();
        model.addAttribute("noBooksFound", bookList.size() == 0);
        model.addAttribute("books", bookList);
        return "books/index";

    }

    @GetMapping("/books/create")
    public String showForm(Model model) {
        model.addAttribute("newBook", new Book());
        return "books/create";
    }

    @PostMapping("/books/create")
    public String createBook(@ModelAttribute Book bookToCreate) {
        bookRepository.save(bookToCreate);
        // redirect the user to the list of ads -> /ads
        return "redirect:/books/";
    }

    @GetMapping("/books/{id}")
    public String showBook(@PathVariable long id, Model model) {
        Book book = bookRepository.getOne(id);
        model.addAttribute("showBook", book);
        return "/books/show";
    }

    @GetMapping("books/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        Book bookToEdit = bookRepository.getOne(id);
        model.addAttribute("editBook", bookToEdit);
        return "books/edit";
    }

    @PostMapping("/books/{id}/edit")
    public String updateBook(@PathVariable long id,
                             @RequestParam(name ="title") String title,
                             @RequestParam(name = "author") String author,
                             @RequestParam(name = "genre") String genre,
                             @RequestParam(name = "releasedDate") String releasedDate) {

        Book foundBook = bookRepository.getBookById(id);

        foundBook.setTitle(title);
        foundBook.setAuthor(author);
        foundBook.setGenre(genre);
        foundBook.setReleasedDate(releasedDate);

        bookRepository.save(foundBook);

        return "redirect:/books/";
    }

    @PostMapping("/books/{id}/delete")
    public String delete(@PathVariable long id) {

        bookRepository.deleteById(id);

        // redirect user to url for list of ads
        return "redirect:/books/";
    }


} // end of class
